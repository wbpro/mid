import { ref, computed } from "vue";
import { defineStore } from "pinia";

export const useCounterStore = defineStore("counter", () => {
  const count = ref(0);
  const doubleCount = computed(() => count.value * 2);
  function increment() {
    count.value++;
  }
  function dcrease() {
    if (count.value === 0) return 0;
    count.value--;
  }

  return { count, doubleCount, dcrease, increment };
});
